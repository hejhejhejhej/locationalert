package com.example.tiami.locationalert;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

/**
 * Created by tiami on 17/11/2017.
 */

public class GeofenceTransitionsIntentService extends IntentService {

    protected static final String TAG = "TransitionIntentService";

    public GeofenceTransitionsIntentService() {
        super(TAG);
    }

    protected void onHandleIntent(Intent intent) {
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            String error = Integer.toString(geofencingEvent.getErrorCode());
            Log.e(TAG, "Geofencing error, with error code: " + error);
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            Log.e(TAG,"You have now entered or exited! TILLYKKE!! ");
            //TODO: this happens when you enter or exit. set or delete notification.
            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(this,"1")
                            .setSmallIcon(R.drawable.mapimg)
                            .setContentTitle("My notification")
                            .setContentText("This is a notification");


            NotificationManager mNotificationManager =

                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            mNotificationManager.notify(1, mBuilder.build());

        }
    }
    }