package com.example.tiami.locationalert;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import java.util.ArrayList;


public class CreateActivity extends AppCompatActivity {

    private GeofencingClient mGeofencingClient;
    private ArrayList<Geofence> gf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        mGeofencingClient = LocationServices.getGeofencingClient(this);
        gf = new ArrayList<>();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
    }

    public void confirmCreation(View view) {

        EditText latitudeView = findViewById(R.id.latitude);
        EditText longitudeView = findViewById(R.id.longitude);
        EditText rangeView = findViewById(R.id.range);
        EditText nameView = findViewById(R.id.nameText);

        double latitude = Double.parseDouble(latitudeView.getText().toString());
        double longitude = Double.parseDouble(longitudeView.getText().toString());
        int range = Integer.parseInt(rangeView.getText().toString());
        final String name = nameView.getText().toString();


        if (name.equals("something from before") /*exists already*/) {

            //TODO: ERROR + ifstatement fix

        } else {

            //create geofence
            final Geofence geo = new Geofence.Builder()

                    .setRequestId(name)

                    .setCircularRegion(
                            latitude,
                            longitude,
                            range //in meter
                    )
                    .setExpirationDuration(Geofence.NEVER_EXPIRE)
                    .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                            Geofence.GEOFENCE_TRANSITION_EXIT)
                    .build();

            gf.add(geo);

            GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
            builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
            builder.addGeofences(gf);
            GeofencingRequest geoRequest = builder.build();

            Intent intent = new Intent(this, GeofenceTransitionsIntentService.class);
            PendingIntent geoIntent = PendingIntent.getService(this, 0, intent, PendingIntent.
                    FLAG_UPDATE_CURRENT);


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        1);
            }

            mGeofencingClient.addGeofences(geoRequest, geoIntent)
                    .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            //save in shared preferences
                            SharedPreferences sharedPref = getPreferences(MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();

                            int currentAmount = sharedPref.getInt("amount",0);
                            String myGeoFence = new Gson().toJson(geo);
                            editor.putString(Integer.toString(currentAmount), myGeoFence);
                            editor.putInt("amount",currentAmount+1);
                            editor.commit();
                            //TODO: something fucked here, not printing what it should
                            System.out.println("committed: " + myGeoFence);
                            System.out.println("AMOUNT = " + currentAmount+1);
                            System.out.println(sharedPref.getInt("amount",0) + " current fences. Last fence is: " + sharedPref.getString(Integer.toString(currentAmount),null));
                            finish();
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            //TODO: ERROR
                        }
                    });
        }

    }
}
